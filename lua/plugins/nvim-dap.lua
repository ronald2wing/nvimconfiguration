return {
  {
    'jayp0521/mason-nvim-dap.nvim',
    config = function()
      require('mason-nvim-dap').setup {
        ensure_installed = { 'php' },
        handlers = {
          function(config)
            require('mason-nvim-dap').default_setup(config)
          end,
          php = function(config)
            config.adapters = { command = 'php-debug-adapter', type = 'executable' }
            config.configurations = {
              {
                name = 'PHP: Listen for Xdebug',
                pathMappings = { ['/app'] = '${workspaceFolder}' },
                port = 9003,
                request = 'launch',
                type = 'php',
              },
            }
            require('mason-nvim-dap').default_setup(config)
          end,
        },
      }
    end,
    dependencies = { 'williamboman/mason.nvim' },
  },
  {
    'mfussenegger/nvim-dap',
    dependencies = {
      'jayp0521/mason-nvim-dap.nvim',
      'nvim-telescope/telescope-dap.nvim',
      'rcarriga/nvim-dap-ui',
      'theHamsta/nvim-dap-virtual-text',
    },
    keys = {
      { '<F5>', "<Cmd>lua require'dap'.continue()<CR>" },
      { '<F10>', "<Cmd>lua require'dap'.step_over()<CR>" },
      { '<F11>', "<Cmd>lua require'dap'.step_into()<CR>" },
      { '<F12>', "<Cmd>lua require'dap'.step_out()<CR>" },
      { '<Leader>b', "<Cmd>lua require'dap'.toggle_breakpoint()<CR>" },
      {
        '<Leader>B',
        "<Cmd>lua require'dap'.set_breakpoint(vim.fn.input('Breakpoint condition: '))<CR>",
      },
      {
        '<Leader>lp',
        "<Cmd>lua require'dap'.set_breakpoint(nil, nil, vim.fn.input('Log point message: '))<CR>",
      },
      { '<Leader>dr', "<Cmd>lua require'dap'.repl.open()<CR>" },
      { '<Leader>dl', "<Cmd>lua require'dap'.run_last()<CR>" },
    },
  },
  {
    'nvim-telescope/telescope-dap.nvim',
    config = function()
      require('telescope').load_extension 'dap'
    end,
    dependencies = { 'nvim-telescope/telescope.nvim' },
  },
  {
    'rcarriga/nvim-dap-ui',
    config = function()
      require('dapui').setup()
      local dap, dapui = require 'dap', require 'dapui'
      dap.listeners.after.event_initialized['dapui_config'] = function()
        dapui.open()
      end
      dap.listeners.before.event_terminated['dapui_config'] = function()
        dapui.close()
      end
      dap.listeners.before.event_exited['dapui_config'] = function()
        dapui.close()
      end
    end,
  },
  {
    'theHamsta/nvim-dap-virtual-text',
    config = true,
  },
}
