return {

  --
  -- Default plugins
  --

  { 'ap/vim-css-color', event = 'VeryLazy' },
  { 'chaoren/vim-imageview', event = 'VeryLazy' },
  {
    'folke/noice.nvim',
    dependencies = { 'MunifTanjim/nui.nvim', 'rcarriga/nvim-notify' },
    lazy = false,
    opts = {
      ['cmp.entry.get_documentation'] = true,
      ['vim.lsp.util.convert_input_to_markdown_lines'] = true,
      ['vim.lsp.util.stylize_markdown'] = true,
    },
  },
  {
    'jackMort/ChatGPT.nvim',
    config = true,
    dependencies = {
      'MunifTanjim/nui.nvim',
      'nvim-lua/plenary.nvim',
      'nvim-telescope/telescope.nvim',
    },
    event = 'VeryLazy',
  },
  { 'j-hui/fidget.nvim', config = true, event = 'VeryLazy' },
  { 'kevinhwang91/nvim-bqf', ft = 'qf' },
  {
    'lukas-reineke/indent-blankline.nvim',
    dependencies = { 'folke/tokyonight.nvim' },
    event = 'VeryLazy',
  },
  {
    'melkster/modicator.nvim',
    config = true,
    event = 'VeryLazy',
    init = function()
      vim.o.cursorline = true
      vim.o.number = true
      vim.o.termguicolors = true
    end,
  },
  {
    'petertriho/nvim-scrollbar',
    config = function()
      require('scrollbar').setup()
      require('scrollbar.handlers.gitsigns').setup()
      require('scrollbar.handlers.search').setup()
    end,
    event = 'VeryLazy',
  },
  { 'preservim/vim-markdown', dependencies = { 'godlygeek/tabular' }, ft = 'markdown' },
  { 'rcarriga/nvim-notify', opts = { background_colour = '#000000' } },
  { 'tpope/vim-repeat', keys = '.' },
  { 'tpope/vim-sensible', event = 'VeryLazy' },
  {
    'utilyre/barbecue.nvim',
    config = true,
    event = 'VeryLazy',
    dependencies = { 'neovim/nvim-lspconfig', 'nvim-tree/nvim-web-devicons', 'SmiteshP/nvim-navic' },
    version = '*',
  }, --
  -- Command plugins
  --
  {
    'aaronhallaert/advanced-git-search.nvim',
    cmd = 'AdvancedGitSearch',
    config = function()
      require('telescope').load_extension 'advanced_git_search'
    end,
    dependencies = {
      'nvim-telescope/telescope.nvim',
      'sindrets/diffview.nvim',
      'tpope/vim-fugitive',
      'tpope/vim-rhubarb',
    },
  },
  {
    'ahmedkhalf/project.nvim',
    config = function()
      require('project_nvim').setup { manual_mode = true }
      require('telescope').load_extension 'projects'
    end,
    dependencies = { 'nvim-telescope/telescope.nvim' },
    event = 'VeryLazy',
  },
  {
    'cbochs/portal.nvim',
    keys = {
      { '<leader>i', '<cmd>Portal jumplist forward<cr>' },
      { '<leader>o', '<cmd>Portal jumplist backward<cr>' },
    },
  },
  { 'chrisbra/csv.vim', ft = 'csv' },
  { 'chrisbra/NrrwRgn', cmd = { 'NR', 'NRL', 'NRM', 'NRN', 'NRP', 'NRS', 'NRV', 'NUD', 'NW', 'WR' } },
  { 'dstein64/vim-startuptime', cmd = 'StartupTime' },
  { 'ellisonleao/weather.nvim', cmd = 'Weather', config = true },
  { 'folke/twilight.nvim', cmd = { 'Twilight', 'TwilightEnable' }, config = true },
  { 'folke/zen-mode.nvim', cmd = 'ZenMode', config = true },
  {
    'ggandor/leap.nvim',
    config = function()
      require('leap').add_default_mappings()
    end,
    keys = { 'S', 's' },
  },
  { 'hsanson/vim-android', event = 'VeryLazy' },
  {
    'iamcco/markdown-preview.nvim',
    build = function()
      vim.fn['mkdp#util#install']()
    end,
    ft = 'markdown',
  },
  {
    'junegunn/vim-easy-align',
    cmd = 'EasyAlign',
    keys = { { 'ga', '<Plug>(EasyAlign)' }, { 'ga', '<Plug>(EasyAlign)', mode = 'x' } },
  },
  {
    'jupyter-vim/jupyter-vim',
    build = {
      '[ ! -e ~/.jupyter/jupyter_console_config.py ] && jupyter console --generate-config',
      'yay -Qi jupyter_console || yay -S jupyter_console',
      'sed -i "s/# c.ZMQTerminalInteractiveShell.include_other_output = False/c.ZMQTerminalInteractiveShell.include_other_output = True/" ~/.jupyter/jupyter_console_config.py',
    },
    cmd = 'JupyterConnect',
  },
  'kana/vim-textobj-user',
  {
    'kdheepak/lazygit.nvim',
    build = 'yay -Qi lazygit-git || yay -S lazygit-git',
    cmd = { 'LazyGit', 'LazyGitFilter' },
    config = function()
      require('telescope').load_extension 'lazygit'
    end,
    dependencies = { 'nvim-lua/plenary.nvim', 'nvim-telescope/telescope.nvim' },
  },
  {
    'kkoomen/vim-doge',
    build = function()
      vim.fn['doge#install']()
    end,
    keys = '<Leader>d',
  },
  { 'kkvh/vim-docker-tools', cmd = 'DockerToolsOpen' },
  { 'kylechui/nvim-surround', config = true, event = 'VeryLazy' },
  { 'lambdalisue/suda.vim', cmd = { 'SudaRead', 'SudaWrite' } },
  { 'lucapette/vim-textobj-underscore', dependencies = { 'kana/vim-textobj-user' }, event = 'VeryLazy' },
  { 'machakann/vim-swap', keys = { 'g<', 'g>', 'gs' } },
  { 'MattesGroeger/vim-bookmarks', keys = { 'ma', 'mc', 'mi', 'mm', 'mn', 'mp', 'mx' } },
  { 'mattn/emmet-vim', event = 'VeryLazy' },
  { 'mg979/vim-visual-multi', branch = 'master', keys = '<C-n>' },
  { 'michaelb/sniprun', build = 'bash install.sh', cmd = { 'SnipInfo', 'SnipRun' } },
  { 'moll/vim-node', event = 'VeryLazy' },
  { 'noahfrederick/vim-composer', event = 'VeryLazy' },
  { 'noahfrederick/vim-laravel', event = 'VeryLazy' },
  { 'numToStr/Comment.nvim', config = true, keys = { 'gcc', { 'gcc', mode = 'x' } } },
  {
    'python-mode/python-mode',
    branch = 'develop',
    config = function()
      vim.g.pymode_python = 'python3'
    end,
    ft = 'python',
  },
  {
    'rest-nvim/rest.nvim',
    config = true,
    dependencies = { 'nvim-lua/plenary.nvim' },
    ft = 'http',
    keys = {
      { '<leader>rn', '<Plug>RestNvim' },
      { '<leader>rnl', '<Plug>RestNvimLast' },
      { '<leader>rnp', '<Plug>RestNvimPreview' },
    },
  },
  {
    'rhysd/devdocs.vim',
    cmd = { 'DevDocs', 'DevDocsAll', 'DevDocsAllUnderCursor', 'DevDocsUnderCursor' },
  },
  { 'skanehira/docker-compose.vim', cmd = 'DockerCompose' },
  { 'skywind3000/asyncrun.vim', cmd = 'AsyncRun' },
  { 'tpope/vim-abolish', event = { 'CmdLineEnter', 'CursorMoved' } },
  { 'tpope/vim-bundler', event = 'VeryLazy' },
  { 'tpope/vim-dadbod', cmd = { 'DB', 'DBUI' }, dependencies = { 'kristijanhusak/vim-dadbod-ui' } },
  { 'tpope/vim-dispatch', event = 'CmdLineEnter' },
  { 'tpope/vim-dotenv', cmd = 'Dotenv' },
  {
    'tpope/vim-eunuch',
    cmd = {
      'Cfind',
      'Chmod',
      'Clocate',
      'Copy',
      'Delete',
      'Duplicate',
      'Lfind',
      'Llocate',
      'Mkdir',
      'Move',
      'Remove',
      'Rename',
      'SudoEdit',
      'SudoWrite',
      'Wall',
    },
  },
  { 'tpope/vim-fireplace', cmd = 'FireplaceConnect' },
  { 'tpope/vim-jdaddy', event = 'VeryLazy' },
  { 'tpope/vim-ragtag', event = 'VeryLazy' },
  { 'tpope/vim-rails', event = 'VeryLazy' },
  { 'tpope/vim-rsi', event = 'VeryLazy' },
  { 'voldikss/vim-browser-search', cmd = 'BrowserSearch' },
  { 'voldikss/vim-translator', cmd = 'Translate' },
  { 'whatyouhide/vim-textobj-xmlattr', dependencies = { 'kana/vim-textobj-user' }, event = 'VeryLazy' },
  {
    'wincent/ferret',
    build = 'yay -Qi ripgrep || yay -S ripgrep',
    cmd = { 'Ack', 'Acks', 'Back', 'Black', 'Lack', 'Lacks', 'Quack' },
  },
}
