local lsp_commons = require 'lsp_commons'

return {
  { 'hrsh7th/cmp-buffer', dependencies = { 'hrsh7th/nvim-cmp' }, event = 'VeryLazy' },
  { 'hrsh7th/cmp-cmdline', dependencies = { 'hrsh7th/nvim-cmp' }, event = 'CmdLineEnter' },
  { 'hrsh7th/cmp-nvim-lsp', dependencies = { 'hrsh7th/nvim-cmp' }, event = 'VeryLazy' },
  { 'hrsh7th/cmp-nvim-lsp-document-symbol', dependencies = { 'hrsh7th/nvim-cmp' }, event = 'VeryLazy' },
  {
    'hrsh7th/cmp-nvim-lsp-signature-help',
    dependencies = { 'hrsh7th/nvim-cmp' },
    event = 'InsertCharPre',
  },
  { 'hrsh7th/cmp-path', dependencies = { 'hrsh7th/nvim-cmp' }, event = 'VeryLazy' },
  {
    'hrsh7th/nvim-cmp',
    config = function()
      require('mason-lspconfig').setup { ensure_installed = lsp_commons.cmp_lsp_servers }

      local cmp = require 'cmp'
      local luasnip = require 'luasnip'
      local has_words_before = function()
        unpack = unpack or table.unpack
        local line, col = unpack(vim.api.nvim_win_get_cursor(0))
        return col ~= 0 and vim.api.nvim_buf_get_lines(0, line - 1, line, true)[1]:sub(col, col):match '%s' == nil
      end
      cmp.setup {
        mapping = cmp.mapping.preset.insert {
          ['<C-b>'] = cmp.mapping.scroll_docs(-4),
          ['<C-f>'] = cmp.mapping.scroll_docs(4),
          ['<C-Space>'] = cmp.mapping.complete(),
          ['<C-e>'] = cmp.mapping.abort(),
          ['<CR>'] = cmp.mapping.confirm { select = true },
          ['<Tab>'] = cmp.mapping(function(fallback)
            if cmp.visible() then
              cmp.select_next_item()
            elseif luasnip.expand_or_jumpable() then
              luasnip.expand_or_jump()
            elseif has_words_before() then
              cmp.complete()
            else
              fallback()
            end
          end, { 'i', 's' }),
          ['<S-Tab>'] = cmp.mapping(function(fallback)
            if cmp.visible() then
              cmp.select_prev_item()
            elseif luasnip.jumpable(-1) then
              luasnip.jump(-1)
            else
              fallback()
            end
          end, { 'i', 's' }),
        },
        snippet = {
          expand = function(args)
            luasnip.lsp_expand(args.body)
          end,
        },
        sources = cmp.config.sources(
          { { name = 'copilot' } },
          { { name = 'nvim_lsp_signature_help' } },
          { { name = 'luasnip' }, { name = 'nvim_lsp' } },
          { { name = 'buffer' }, { name = 'path' } }
        ),
      }
      cmp.setup.cmdline({ '/', '?' }, {
        mapping = cmp.mapping.preset.cmdline(),
        sources = cmp.config.sources { { name = 'buffer' }, { name = 'nvim_lsp_document_symbol' } },
      })
      cmp.setup.cmdline(':', {
        mapping = cmp.mapping.preset.cmdline(),
        sources = cmp.config.sources { { name = 'cmdline' }, { name = 'path' } },
      })
      cmp.setup.filetype('gitcommit', {
        sources = cmp.config.sources {
          {
            name = 'buffer',
            option = {
              get_bufnrs = function()
                return vim.api.nvim_list_bufs()
              end,
            },
          },
          { name = 'nvim_lsp_document_symbol' },
          { name = 'path' },
        },
      })
    end,
    dependencies = {
      'neovim/nvim-lspconfig',
      'williamboman/mason-lspconfig.nvim',
      'zbirenbaum/copilot-cmp',
    },
  },
  {
    'L3MON4D3/LuaSnip',
    config = function()
      require('luasnip.loaders.from_lua').lazy_load()
    end,
    dependencies = { 'rafamadriz/friendly-snippets' },
    version = 'v1.*',
  },
  {
    'saadparwaiz1/cmp_luasnip',
    dependencies = { 'hrsh7th/nvim-cmp', 'L3MON4D3/LuaSnip' },
    event = 'InsertCharPre',
  },
  { 'zbirenbaum/copilot-cmp', config = true },
}
