vim.api.nvim_set_keymap('i', '...', '…', { noremap = true })
vim.api.nvim_set_option('number', true)
vim.api.nvim_set_option('relativenumber', true)
vim.api.nvim_set_option('shell', 'zsh')
