local M = {}
M.cmp_lsp_servers = {
  'clojure_lsp', -- Clojure
  'quick_lint_js', -- JavaScript
  'lua_ls', -- Lua
  'marksman',
  'prosemd_lsp', -- Markdown
  'intelephense',
  'psalm', -- PHP
  'ruby_lsp', -- Ruby
  'somesass_ls', -- Sass
}
M.nullls_lsp_servers = {
  'clojure_lsp',
  'joker', -- Clojure
  -- 'lua-language-server', 'luacheck', 'selene',
  'luaformatter',
  'stylua', -- Lua
  -- 'phpmd',
  'intelephense',
  'phpactor',
  'phpcs',
  'phpcsfixer',
  'phpstan',
  'pint',
  'psalm', -- PHP
  'biome',
  'deno',
  'eslint-lsp',
  'prettier',
  'rustywind', -- JavaScript
  'proselint',
  'remark-cli', -- Markdown
  'clang-format',
  'editorconfig_checker', -- Misc
  'erb-lint', -- Ruby
  'standardrb',
}
M.lsp_servers = {}
for _, v in ipairs(M.cmp_lsp_servers) do
  table.insert(M.lsp_servers, v)
end
--[[ for _, v in ipairs(vim.g.nullls_lsp_servers) do
	table.insert(lsp_servers, v)
end ]]

return M
