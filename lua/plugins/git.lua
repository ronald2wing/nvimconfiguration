return {
  { 'junegunn/gv.vim', cmd = { 'GV' }, dependencies = { 'tpope/vim-fugitive' } },
  {
    'lewis6991/gitsigns.nvim',
    event = 'VeryLazy',
    opts = {
      on_attach = function(bufnr)
        local gs = package.loaded.gitsigns

        local function map(mode, l, r, opts)
          opts = opts or {}
          opts.buffer = bufnr
          vim.keymap.set(mode, l, r, opts)
        end

        -- Navigation
        map('n', ']c', function()
          if vim.wo.diff then
            return ']c'
          end
          vim.schedule(function()
            gs.next_hunk()
          end)
          return '<Ignore>'
        end, { expr = true })

        map('n', '[c', function()
          if vim.wo.diff then
            return '[c'
          end
          vim.schedule(function()
            gs.prev_hunk()
          end)
          return '<Ignore>'
        end, { expr = true })

        -- Actions
        map({ 'n', 'v' }, '<leader>hs', ':Gitsigns stage_hunk<CR>')
        map({ 'n', 'v' }, '<leader>hr', ':Gitsigns reset_hunk<CR>')
        map('n', '<leader>hS', gs.stage_buffer)
        map('n', '<leader>hu', gs.undo_stage_hunk)
        map('n', '<leader>hR', gs.reset_buffer)
        map('n', '<leader>hp', gs.preview_hunk)
        map('n', '<leader>hb', function()
          gs.blame_line { full = true }
        end)
        map('n', '<leader>tb', gs.toggle_current_line_blame)
        map('n', '<leader>hd', gs.diffthis)
        map('n', '<leader>hD', function()
          gs.diffthis '~'
        end)
        map('n', '<leader>td', gs.toggle_deleted)

        -- Text object
        map({ 'o', 'x' }, 'ih', ':<C-U>Gitsigns select_hunk<CR>')
      end,
    },
  },
  { 'rhysd/git-messenger.vim', cmd = 'GitMessager', keys = '<Leader>gm' },
  'tommcdo/vim-fubitive',
  {
    'tpope/vim-fugitive',
    cmd = {
      'G',
      'GBrowse',
      'GDelete',
      'GMove',
      'Gdiffsplit',
      'Git',
      'Grep',
      'Gsplit',
      'Gstatus',
      'Gwrite',
    },
    config = function()
      vim.api.nvim_create_user_command('Browse', function(opts)
        vim.fn.system { 'xdg-open', opts.fargs[1] }
      end, { nargs = 1 })
    end,
    dependencies = { 'tommcdo/vim-fubitive', 'tpope/vim-rhubarb' },
  },
  'tpope/vim-rhubarb',
}
