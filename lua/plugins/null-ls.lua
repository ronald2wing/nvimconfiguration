local lsp_commons = require 'lsp_commons'

return {
  {
    'jayp0521/mason-null-ls.nvim',
    config = function()
      require('mason-null-ls').setup {
        ensure_installed = lsp_commons.nullls_lsp_servers,
        handlers = {},
      }
    end,
    dependencies = { 'williamboman/mason.nvim' },
  },
  {
    'jose-elias-alvarez/null-ls.nvim',
    dependencies = { 'jayp0521/mason-null-ls.nvim', 'nvim-lua/plenary.nvim' },
    event = 'VeryLazy',
    init = function()
      vim.opt.completeopt = { 'menu', 'menuone', 'noselect' }
    end,
    opts = function()
      local null_ls = require 'null-ls'
      return {
        sources = {
          null_ls.builtins.diagnostics.php, -- PHP
          null_ls.builtins.code_actions.gitsigns,
          null_ls.builtins.code_actions.refactoring.with { extra_filetypes = { 'php', 'ruby' } },
          null_ls.builtins.hover.dictionary, -- Misc
        },
      }
    end,
  },
}
