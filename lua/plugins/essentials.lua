return {
  { 'akinsho/toggleterm.nvim', cmd = 'ToggleTerm', config = true, version = '*' },
  {
    'amitds1997/remote-nvim.nvim',
    build = 'yay -Qi devpod-bin || yay -S devpod-bin',
    cmd = 'RemoteStart',
    dependencies = {
      'MunifTanjim/nui.nvim',
      'nvim-lua/plenary.nvim',
      'nvim-telescope/telescope.nvim',
    },
    opts = {
      devpod = {
        binary = 'devpod-cli',
      },
    },
    version = '*',
  },
  { 'artnez/vim-wipeout', cmd = 'Wipeout' },
  {
    'christoomey/vim-system-copy',
    event = 'VeryLazy',
  },
  {
    'epwalsh/obsidian.nvim',
    build = 'yay -Qi obsidian || yay -S obsidian',
    cmd = {
      'ObsidianNew',
      'ObsidianOpen',
      'ObsidianQuickSwitch',
      'ObsidianSearch',
      'ObsidianToday',
      'ObsidianWorkspace',
    },
    dependencies = { 'nvim-lua/plenary.nvim' },
    ft = 'markdown',
    opts = {
      daily_notes = { folder = 'daily notes' },
      notes_subdir = 'slip-box',
      templates = { subdir = 'templates' },
      workspaces = {
        { name = 'personal', path = '~/vaults/personal' },
        { name = 'work', path = '~/vaults/work' },
      },
    },
    version = '*',
  },
  {
    'folke/tokyonight.nvim',
    config = function()
      vim.cmd 'colorscheme tokyonight'
    end,
    event = 'VeryLazy',
  },
  {
    'folke/trouble.nvim',
    config = true,
    dependencies = { 'nvim-tree/nvim-web-devicons' },
    keys = {
      { '<leader>xx', '<cmd>TroubleToggle<cr>', silent = true, noremap = true },
      { '<leader>xw', '<cmd>TroubleToggle workspace_diagnostics<cr>', silent = true, noremap = true },
      { '<leader>xd', '<cmd>TroubleToggle document_diagnostics<cr>', silent = true, noremap = true },
      { '<leader>xl', '<cmd>TroubleToggle loclist<cr>', silent = true, noremap = true },
      { '<leader>xq', '<cmd>TroubleToggle quickfix<cr>', silent = true, noremap = true },
      { 'gR', '<cmd>TroubleToggle lsp_references<cr>', silent = true, noremap = true },
    },
  },
  {
    'folke/which-key.nvim',
    cmd = 'WhichKey',
    init = function()
      vim.o.timeout = true
      vim.o.timeoutlen = 300
    end,
  },
  {
    'kevinhwang91/nvim-hlslens',
    config = true,
    keys = {
      { 'n', "<Cmd>execute('normal! ' . v:count1 . 'n')<CR><Cmd>lua require('hlslens').start()<CR>" },
      { 'N', "<Cmd>execute('normal! ' . v:count1 . 'N')<CR><Cmd>lua require('hlslens').start()<CR>" },
      { '*', "*<Cmd>lua require('hlslens').start()<CR>" },
      { '#', "#<Cmd>lua require('hlslens').start()<CR>" },
      { 'g*', "g*<Cmd>lua require('hlslens').start()<CR>" },
      { 'g#', "g#<Cmd>lua require('hlslens').start()<CR>" },
      { '<Leader>l', '<Cmd>noh<CR>' },
    },
  },
  {
    'nvim-lualine/lualine.nvim',
    dependencies = {
      'f-person/git-blame.nvim',
      'folke/tokyonight.nvim',
      'nvim-tree/nvim-web-devicons',
    },
    event = 'VeryLazy',
    init = function()
      vim.g.gitblame_display_virtual_text = 0
    end,
    opts = function()
      local git_blame = require 'gitblame'
      return {
        extensions = { 'fugitive', 'man', 'nvim-dap-ui', 'quickfix', 'toggleterm' },
        sections = {
          lualine_c = { { git_blame.get_current_blame_text, cond = git_blame.is_blame_text_available } },
        },
        tabline = {
          lualine_a = { 'tabs' },
          lualine_y = { 'buffers' },
          lualine_z = { 'os.date("%c", os.time())' },
        },
      }
    end,
  },
  {
    'nvim-telescope/telescope-fzf-native.nvim',
    build = 'make',
    config = function()
      require('telescope').load_extension 'fzf'
    end,
  },
  {
    'nvim-telescope/telescope.nvim',
    build = 'yay -Qi ripgrep || yay -S ripgrep',
    dependencies = { 'nvim-lua/plenary.nvim', 'nvim-telescope/telescope-fzf-native.nvim' },
    keys = {
      { '<C-g>', '<cmd>Telescope live_grep<Cr>' },
      { '<C-f>', '<cmd>Telescope find_files find_command=rg,--files,--hidden,--no-ignore-vcs<Cr>' },
    },
    opts = function()
      local trouble = require 'trouble.sources.telescope'

      return {
        defaults = {
          layout_config = { height = 0.8, mirror = true, width = 0.99 },
          layout_strategy = 'vertical',
          mappings = {
            i = { ['<c-t>'] = trouble.open },
            n = { ['<c-t>'] = trouble.open },
          },
        },
      }
    end,
  },
  {
    'rgroli/other.nvim',
    cmd = { 'Other', 'OtherSplit', 'OtherTabNew', 'OtherVSplit' },
    config = function()
      require('other-nvim').setup { mappings = { 'angular', 'golang', 'laravel', 'livewire', 'rails' } }
    end,
  },
  {
    'ribelo/taskwarrior.nvim',
    build = 'yay -Qi task || yay -S task',
    cmd = 'Task',
    config = true,
    keys = { {
      '<C-t>',
      function()
        require('taskwarrior_nvim').browser { 'ready' }
      end,
    } },
  },
  {
    'Shatur/neovim-session-manager',
    dependencies = { 'nvim-lua/plenary.nvim' },
    event = 'VimEnter',
    opts = function()
      return { autoload_mode = require('session_manager.config').AutoloadMode.CurrentDir }
    end,
  },
  { 'sindrets/diffview.nvim', cmd = { 'DiffviewFileHistory', 'DiffviewOpen' } },
  {
    'stevearc/oil.nvim',
    keys = { { '-', ":lua require('oil').open()<Cr>", desc = 'Open parent directory' } },
    opts = {
      keymaps = { ['!'] = 'actions.open_cmdline', ['.'] = 'actions.copy_entry_path' },
      view_options = { show_hidden = true },
    },
  },
  { 'tpope/vim-unimpaired', event = 'VeryLazy' },
  { 'voldikss/vim-floaterm', cmd = 'FloatermNew' },
  { 'zbirenbaum/copilot.lua', cmd = 'Copilot', config = true, event = 'InsertCharPre' },
}
