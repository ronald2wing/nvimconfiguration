return {
  {
    'andymass/vim-matchup',
    config = function()
      require('nvim-treesitter.configs').setup { matchup = { enable = true } }
    end,
    dependencies = { 'nvim-treesitter/nvim-treesitter' },
    event = 'BufReadPost',
  },
  {
    'kevinhwang91/nvim-ufo',
    dependencies = { 'kevinhwang91/promise-async', 'nvim-treesitter/nvim-treesitter' },
    event = 'BufEnter',
    init = function()
      vim.cmd 'au FileType markdown UfoDetach'
      vim.o.foldcolumn = '1'
      vim.o.foldenable = true
      vim.o.foldlevel = 99
      vim.o.foldlevelstart = 99
    end,
    opts = {
      provider_selector = function(bufnr, filetype, buftype)
        return { 'indent', 'treesitter' }
      end,
    },
  },
  {
    'nvim-neotest/neotest',
    dependencies = {
      'antoinemadec/FixCursorHold.nvim',
      'nvim-lua/plenary.nvim',
      'nvim-treesitter/nvim-treesitter',
    },
    event = 'CmdLineEnter',
  },
  {
    'nvim-treesitter/nvim-treesitter',
    build = ':TSUpdate',
    config = function()
      require('nvim-treesitter.configs').setup {
        ensure_installed = 'all',
        highlight = { enable = true },
        incremental_selection = { enable = true },
        indent = { enable = true },
        matchup = { enable = true },
      }
    end,
  },
  {
    'nvim-treesitter/nvim-treesitter-context',
    config = true,
    dependencies = { 'nvim-treesitter/nvim-treesitter' },
    event = 'VeryLazy',
  },
  {
    'stevearc/aerial.nvim',
    config = function()
      require('aerial').setup { open_automatic = true }
      require('telescope').load_extension 'aerial'
    end,
    dependencies = { 'nvim-telescope/telescope.nvim', 'nvim-treesitter/nvim-treesitter' },
    event = 'VeryLazy',
  },
  {
    'Wansmer/treesj',
    config = true,
    dependencies = { 'nvim-treesitter/nvim-treesitter' },
    keys = { '<space>j', '<space>m', '<space>s' },
  },
}
